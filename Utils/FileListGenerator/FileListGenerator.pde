File dir;
void setup(){
	// The folder in Tablet/data that will be listed
	String path = "img";


	// tabletdatapath is the absolute path to the Tablet/data folder
	String [] info = loadStrings("tabletdatapath.txt");

	String mainpath = info[0];


	String pathtolist = mainpath+path;

	dir = new File(pathtolist);

	String [] files = dir.list();

	if(dir.exists()){
		println("Listing directory: "+pathtolist);
		for(String f : files){
			println(f);
		}

		println("Saving file...");
		saveStrings(String.format("%s/%s.txt",mainpath,path),files);

		println("File saved!");


	}

	exit();


}

void draw(){


}
