/**
 * oscP5broadcaster by andreas schlegel
 * an osc broadcast server.
 * osc clients can connect to the server by sending a connect and
 * disconnect osc message as defined below to the server.
 * incoming messages at the server will then be broadcasted to
 * all connected clients. 
 * an example for a client is located in the oscP5broadcastClient exmaple.
 * oscP5 website at http://www.sojamo.de/oscP5
 */
 
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddressList myNetAddressList = new NetAddressList();
/* listeningPort is the port the server is listening for incoming messages */
int myListeningPort = 4815;
/* the broadcast port is the port the clients should listen for incoming messages from the server*/
int myBroadcastPort = 4815;

String myConnectPattern = "/server/connect";
String myDisconnectPattern = "/server/disconnect";

ArrayList<Command> coms;
int currentCom;
int currentTarget;
int currentArgs;

int setPointer;

final int SET_TARGET_ID = 0;
final int SET_ADDRESS = 1;
final int SET_ARGS = 2;


void setup() {
	size(800,600);
  oscP5 = new OscP5(this, myListeningPort);

  coms = new ArrayList<Command>();
  addCommands();

  currentCom = 0;
  currentTarget = 0;
  currentArgs = 0;
}

void draw() {
  background(0);
}

void mousePressed(){
	sendMessage("image","next");
}

void keyPressed(){
	switch(key){
		case 'm':
			Command com = coms.get(currentCom);
			println("Sending the following message...");
			printCurrentState();
			println();
			sendMessage(com.mainAddr,com.secAddr,currentTarget,currentArgs);
			println("Sent!");
		break;

		case 't':
			println("Changing Target ID!");
			setPointer = SET_TARGET_ID;
		break;
		case 'a':
			println("Changing Address!");
			setPointer = SET_ADDRESS;
			printAddressTable();
		break;
		case 'g':
			setPointer = SET_ARGS;
			println("Changing arg!");
		break;

		case 'p':
			printCurrentState();
		break;
	}
	switch(setPointer){
		case SET_TARGET_ID:
			if(key>='0' && key<='9'){
				currentTarget = key-'0';
				println("Current Target changed!");
				println(currentTarget);
			}
		break;
		case SET_ARGS:
		// !!! this has to be changed
			if(key>='0' && key<='9'){
				currentArgs = key-'0';
				println("Current Arg changed!");
				println(currentArgs);
			}
		break;
		case SET_ADDRESS:
			int newCurrentCom = 0;
			while(newCurrentCom<coms.size() && !coms.get(newCurrentCom).isKey(key)){
				newCurrentCom++;
			}
			if(newCurrentCom<coms.size()){
				currentCom = newCurrentCom;
				println("Current Command changed!");
				println(coms.get(newCurrentCom).addrToString());
			}

		break;

	}


}

void printCurrentState(){
	println("Current message state:");
	println(String.format("Target: %d\nAddress:%s\nArg:%d",currentTarget,coms.get(currentCom).addrToString(),currentArgs));

}

void printAddressTable(){
	for(Command c : coms){
		println(c.toString());
	}
}

void sendMessage(String mainAddr, String secAddr){
	sendMessage(mainAddr,secAddr,0);
}

void sendMessage(String mainAddr, String secAddr, int arg){
	sendMessage(mainAddr,secAddr,0,arg);
}

void sendMessage(String mainAddr, String secAddr, int destId, int arg){
	OscMessage msg = new OscMessage(String.format("/%s/%s",mainAddr,secAddr));

	msg.add(destId);
	msg.add(arg);

	println("Sending the following message:");
	msg.print();

	oscP5.send(msg,myNetAddressList);
}

void oscEvent(OscMessage theOscMessage) {
  /* check if the address pattern fits any of our patterns */
  if (theOscMessage.addrPattern().equals(myConnectPattern)) {
    connect(theOscMessage.netAddress().address());
  }
  else if (theOscMessage.addrPattern().equals(myDisconnectPattern)) {
    disconnect(theOscMessage.netAddress().address());
  }
  /**
   * if pattern matching was not successful, then broadcast the incoming
   * message to all addresses in the netAddresList. 
   */
  else {
    oscP5.send(theOscMessage, myNetAddressList);
  }
}


 private void connect(String theIPaddress) {
     if (!myNetAddressList.contains(theIPaddress, myBroadcastPort)) {
       myNetAddressList.add(new NetAddress(theIPaddress, myBroadcastPort));
       println("### adding "+theIPaddress+" to the list.");
     } else {
       println("### "+theIPaddress+" is already connected.");
     }
     println("### currently there are "+myNetAddressList.list().size()+" remote locations connected.");
 }



private void disconnect(String theIPaddress) {
if (myNetAddressList.contains(theIPaddress, myBroadcastPort)) {
		myNetAddressList.remove(theIPaddress, myBroadcastPort);
       println("### removing "+theIPaddress+" from the list.");
     } else {
       println("### "+theIPaddress+" is not connected.");
     }
       println("### currently there are "+myNetAddressList.list().size());
 }

 void addCommands(){
 	coms.add(new Command('I',"config","setstateimg"));
 	coms.add(new Command('S',"config","setstatestream"));
	coms.add(new Command('Z',"image","prev"));
	coms.add(new Command('X',"image","next"));
	coms.add(new Command('C',"image","select"));
 }
