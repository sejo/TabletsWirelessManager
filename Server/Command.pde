// Class to wrap a key and its corresponding 
// mainAddr/destAddr
class Command{
	public char k;
	public String mainAddr;
	public String secAddr;

	Command(char ke, String m, String s){
		k = ke;
		mainAddr = m;
		secAddr = s;
	}

	public String toString(){
		return String.format("%c\t/%s/%s",k,mainAddr,secAddr);
	}

	public String addrToString(){
		return String.format("/%s/%s",mainAddr,secAddr);
	}

	public boolean isKey(char ke){
		return k==ke;
	}


}
