class PathManager{
	String path;

	ArrayList<String> filenames;

	int filepointer;

	PathManager(String p){
		path = p;

		filenames = new ArrayList<String>();

		loadFiles();

		filepointer = 0;
	}

	void loadFiles(){
		String [] files = loadStrings(path+".txt");

		filenames.clear();

		for(String f : files){
			filenames.add(f);
		}
	}

	public String getCurrentFilePath(){
		return path+"/"+filenames.get(filepointer);
	}

	public PImage getCurrentPImage(){
		PImage img = loadImage(getCurrentFilePath());
		return img;
	}

	public int numFiles(){
		return filenames.size();
	}

	public void nextFile(){
		filepointer = (filepointer+1)%numFiles();
	}

	public void prevFile(){
		filepointer = (numFiles() + filepointer-1)%numFiles();
	}

	public void selectFile(int i){
		filepointer = i;
	}


}
