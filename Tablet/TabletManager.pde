class TabletManager{
	Config config;
	DisplayManager display;


	// Address of the broadcast server
	NetAddress broadcastLocation;

	// Constructor
	TabletManager(PApplet pa, String configfilename){
		// Create configuration
		config = new Config(configfilename);

		// Set the location of the server
		broadcastLocation = new NetAddress(config.getServerAddress(), config.getOscPort());

		// Create display manager
		display = new DisplayManager();

		display.setIPCapture(pa,config.getStreamAddress());

	}


	// Connect to OSC Broadcast server
	public void connect(){
		println("Connecting to OSC server...");
		OscMessage m = new OscMessage("/server/connect");
		oscP5.send(m,broadcastLocation);

	}
	// Disconnect from OSC Broadcast server
	public void disconnect(OscP5 oscP5){
		println("Disconnecting of OSC server...");
		OscMessage m = new OscMessage("/server/disconnect");
		oscP5.send(m,broadcastLocation);
	}

	// Draw tablet screen
	public void draw(){
		display.draw();
		/*
		fill(255,0,0);
		textSize(50);
		text(config.toString(),100,100);
		*/
	}

	// Manage pressing
	public void pressed(int x, int y){
		int p=display.pressed(x,y);

		if(p>0){
			switch(p){
				// Connect to OSC server
				case 1:
					connect();	
				break;

			}

		}
	}


	// Parse message and take appropriate action
	public void parseMessage(OscMessage msg){
		String addr = msg.addrPattern();

		// Check if the message is appropriate to get parsed
		if(msg.arguments()!=null && msg.arguments().length>0){
			// Get destination Id
			int destId = msg.get(0).intValue();

			// Parse message only if it is directed to this tablet
			if(destId == 0 || destId == config.getId()){
	
				String [] tokens = split(addr,'/');
	
				if(tokens.length > 2){
					String mainAddr = tokens[1];
					String secAddr = tokens[2];
	
					// Messages and actions
					if(mainAddr.equals("config")){
						if(secAddr.equals("setstate")){
							display.setState(msg.get(1).intValue());
						}
						if(secAddr.equals("setstateimg")){
							display.setStateImg();
						}
						if(secAddr.equals("setstatestream")){
							display.setStateStream();
						}
						if(secAddr.equals("setstateconfig")){
							display.setStateConfig();
						}
					}
					if(mainAddr.equals("videostream")){
	
	
					}
					else if(mainAddr.equals("image") && display.isStateImg() ){
						if(secAddr.equals("next")){
							display.loadNextImg();
						}
						if(secAddr.equals("prev")){
							display.loadPrevImg();
						}
						if(secAddr.equals("select")){
							display.loadImg(msg.get(1).intValue());
						}
					}
	
				}
	
			}
		}
	}


	// Get OSC port for oscP5 configuration
	public int getOscPort(){
		return config.getOscPort();
	}

	public void printConfig(){
		println(config.toString());
	}




}
