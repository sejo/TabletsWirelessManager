import oscP5.*;
import netP5.*;
import ipcapture.*;

TabletManager manager;

OscP5 oscP5;

void setup(){
	size(800,1280,P2D);
	orientation(PORTRAIT);
	manager = new TabletManager(this,"config/config.txt");
	manager.printConfig();

	oscP5 = new OscP5(this,manager.getOscPort());
	manager.connect();
}


void draw(){
	manager.draw();

}

void mousePressed(){
	/*
	manager.connect(oscP5);
	*/
	manager.pressed(mouseX,mouseY);
}

void oscEvent(OscMessage theOscMessage) {
	manager.parseMessage(theOscMessage);
    	theOscMessage.print();
 }
