// Configuration data for the tablet
class Config{
	int id;
	String serveraddr;
	String streamaddr;
	int oscport;

	String filename;

	Config(String f){
		filename = f;
		load();
	}

	Config(){
		this("config/config.txt");
	}


	// Load configuration from file
	public void load(){
		String [] strings = loadStrings(filename);

		id = Integer.parseInt(strings[0]);
		serveraddr = strings[1];
		streamaddr = strings[2];
		oscport = Integer.parseInt(strings[3]);
	}

	// Config to string
	public String toString(){
		return String.format("Tablet config\nId:%d\nServer addr:%s\nStream addr:%s\nOSC Port:%d",id,serveraddr,streamaddr,oscport);
	}

	// Save configuration in file
	public void store(){
		String [] strings = {""+id, serveraddr, streamaddr,""+oscport};

		saveStrings(filename, strings);
	}

	// Setters
	public void setId(int i){
		id = i;
	}

	public void setServerAddress(String a){
		serveraddr = a;
	}

	public void setStreamAddress(String a){
		streamaddr = a;
	}

	public void setOscPort(int p){
		oscport = p;
	}

	// Getters
	public int getId(){
		return id;
	}

	public String getServerAddress(){
		return serveraddr;
	}

	public String getStreamAddress(){
		return streamaddr;
	}

	public int getOscPort(){
		return oscport;
	}



}
