class DisplayManager{
	final int N_STATES = 4;
	final int STATE_CONFIG = 3;
	final int STATE_IMG = 0;
	final int STATE_STREAM = 1;
	final int STATE_VIDEO = 2;

	final color C_BLUE = color(0,30,97);
	final color C_RED = color(229,62,48);
	final color C_GRAY = color(121,102,105);

	int state;

	PImage img;
	int imgindex;

	PathManager images;

	IPCapture stream;
	
	DisplayManager(){
		state = STATE_IMG;



		images = new PathManager("img");
		println(images.numFiles());

		println(images.getCurrentFilePath());


		img = images.getCurrentPImage();

	}

	public void setIPCapture(PApplet pa, String address){
		stream = new IPCapture(pa,address,"","");
		stream.start();
	}


	public void draw(){
//		background(C_BLUE);
		background(0);

		switch(state){
			case STATE_CONFIG:
				fill(C_RED);
				textSize(50);
				text("Config",100,100);

			break;

			case STATE_IMG:
				drawCenteredImage(img);
			break;


			case STATE_STREAM:
				if(stream.isAvailable()){
					stream.read();
				}

				/*
				translate(-100,-100);
				scale(1.2);
				*/
				drawCenteredImage(stream);
			break;

		}


	}

	private void drawCenteredImage(PImage i){
		int w = i.width;
		int h = i.height;

	/*	float nw = width;
		float sf = 1.0*nw/w;
		float nh = 1.0*h*sf;*/

    float nh = width;
    float sf = 1.0*nh/h;
    float nw = 1.0*w*sf;

//		translate(0,(height-nh)/2);
            translate(0,nw);
            rotate(-PI/2);
		scale(sf);
		image(i,0,0);


	}

	// Manage pressing
	// Returns negative if it's an internal operation, a positive int otherwise
	// So that it can be handled 
	public int pressed(int x, int y){
		switch(state){
			case STATE_CONFIG:
				// (Re)connect to OSC server (1)
				if(isRectPressed(x,y,width*4/9,0, width/9, height/8)){
					return 1;
				}
			break;

			case STATE_IMG:
				if(isRectPressed(x,y,width*2/3, height*5/8, width/6, height/8)){
					loadNextImg();
					return -1;
				}

			break;

		}

		// Next state
		if(isRectPressed(x,y,width*4/9,height*7/8, width/9, height/8)){
			nextState();
			return -1;
		}

		return 0;

	}

	private boolean isRectPressed(float px, float py, float x, float y, float w, float h){
		return (px>=x && px<=x+w && py>=y && py<=y+h);

	}

	// Control methods
	public void setState(int s){
		state = s;
	}

	public void setStateConfig(){
		state = STATE_CONFIG;
	}
	public void setStateImg(){
		state = STATE_IMG;
	}
	public void setStateStream(){
		state = STATE_STREAM;
	}

	public boolean isStateConfig(){
		return state==STATE_CONFIG;
	}
	public boolean isStateImg(){
		return state==STATE_IMG;
	}
	public boolean isStateStream(){
		return state==STATE_STREAM;
	}

	public int getState(){
		return state;
	}

	public int nextState(){
		state = (state+1)%N_STATES;
		return state;
	}

	public int prevState(){
		state = (N_STATES+state-1)%N_STATES;
		return state;
	}

	public void loadNextImg(){
		images.nextFile();

		img = images.getCurrentPImage();
	}

	public void loadPrevImg(){
		images.prevFile();

		img = images.getCurrentPImage();
	}

	public void loadImg(int i){
		images.selectFile(i);

		img = images.getCurrentPImage();
	}




}