# TabletsWirelessManager

There is a "Server" sketch that is executed from a computer and that is prepared to receive keystrokes to control the tablets, and there is a "Tablet" sketch that should be downloaded and executed from each tablet. The "Tablet" sketch has a config file that can be modified for each tablet, specially to change its "id" so that it can be addressable independently. 

All the connection protocol is based in the oscP5broadcaster (server and client) examples of the [oscP5 library](http://www.sojamo.de/libraries/oscp5/), so that's another place to look at: [http://www.sojamo.de/libraries/oscp5/#examples](http://www.sojamo.de/libraries/oscp5/#examples) (the tablets would be the clients, the server the control computer)

